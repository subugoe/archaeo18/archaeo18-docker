Archaeo 18 Docker
=================

The Archaeo 18 application provided as a set of Docker images.


Contents
========

The original README file had become quite long, so it had been sprit up

  - [Status](./docs/experimental.md#status) 

  - Containers
  
    - [`web` container](./docs/web.md)
    
    - [`search` container](./docs/search.md)
    
    - [`images` container](./docs/images.md)
    
    - [`data` container](./docs/data.md)
    
    - [Shared scripts](./docs/shared-scripts.md)
    
  - [Using `docker-compose`](./docs/docker-compose.md)    
    
  - [Setting up a production instance](./docs/production.md)

  - [Experimental features](./docs/experimental.md#experimental)

Quick start
===========

**This quick start guide is for developers only! For setting this up for production see the [document](./docs/production.md).**

## Prerequisites (software and configuration)

The following software is needed, use the software management of your OS to get these:

- [Git](https://git-scm.com/), to get the sources.

- [Docker](https://www.docker.com/), make sure you have a version same as or newer then 18.09

- [Bash](https://www.gnu.org/software/bash/), is needed to run several scripts

To build the images you need Docker to [allow experimental features](https://docs.docker.com/engine/reference/commandline/dockerd/#description).

## Checkout the repository

To avoid the need to have a `git` client in each container this repository contains it's 
dependencies as submodules, you need to check them out first:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone --recurse-submodules -j8 https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker.git
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** If `git` complains that `-j` isn't supported, your version is to old. It's completely safe to omit this option.

**Note:** Make sure you check out everything in a directory named `archaeo18-docker` if you want to use the [experimental extensions](./docs/experimental.md#experimental).

## Build and start containers

You can run the builds and start in one line after you changed into the checkout directory:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh && docker-compose up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For more information (like changing the host name) on `docker-compose-build.sh` see [below](./docs/docker-compose.md#docker-compose-build.sh). 
If you get a timeout during running `docker-compose up` see [below](#note_compose) on how to increase the timeout.

Slow start
==========

### Different Docker usage

Docker contains a serious design flaw regarding the `COPY` command, see the [issue tracker](https://github.com/moby/moby/issues/15858), 
there is a [solution](https://github.com/moby/moby/issues/39530#issuecomment-511815220) which requires a fairly new Docker (18.09 or newer).

To build these images you need Docker (same as or newer then 18.09) to [allow experimental features](https://docs.docker.com/engine/reference/commandline/dockerd/#description).

You need to run `docker build` with the following prefix to use BuildKit's experimental features:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Be aware, that `docker-compose` doesn't understand how to build with BuildKit yet! [See below](./docs/docker-compose.md#docker-compose)

To get every output of `docker build` commands append the following to the `build` command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--progress=plain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Setup the host name for deployment

To change the host name of the `web` container for deployment in `.env` by setting `DEPLOY_HOST_NAME`. By changing 
this variable the new host name will be passed to the web container. You can also change the value in the `environment` 
section of the `docker-compose.yml` file. Also make sure that the exposed port in this file matches.

If you use a external Nginx configuration file (like `docker/web/conf/site.conf`) you need to change the host name there as well.

## Build the containers with data files

The first step is to build the `data` and `image` container. This speeds up the next build steps if you plan to change something.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_data -f docker/data/Dockerfile .
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_images -f docker/images/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Build the `web` and `search`

The second step is to build the `web` and `search` container. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_search -f docker/search/Dockerfile .
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_web -f docker/web/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Start required containers

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose up --build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you get a timeout during running `docker-compose up` see [below](#note_compose) on how to increase the timeout.

## Accessing the `web` frontend

Point your browser to [http://localhost:8008/index.html](http://localhost:8008/index.html) - make sure the port number matches your configuration.

Technical Background
====================

# Building with BuildKit

Since all images are build using BuildKit they all have in common that during the build the contents of the build directory (`.`) 
is mounted as `/mnt/build-context`. This allows the usage of normal `cp` instructions instead of Docker `COPY`, which is more flexible and less error prone.

The different containers are described in their own files now:

  - [`web` container](./docs/web.md)
    
  - [`search` container](./docs/search.md)
    
  - [`images` container](./docs/images.md)
    
  - [`data` container](./docs/data.md)

  - [Shared scripts](./docs/shared-scripts.md)

<a name="devel"></a>
Setting up a development instance
=================================

# Running with exposed services

It's possible to start the containers with Solr and Kitodo ContentServer exposed, just run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --no-build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Make sure you've build the images beforehand.

And point your browser to one of the following URLs.

| Container                       | URL                                                            | 
|---------------------------------|----------------------------------------------------------------|
| Solr (`search`)                 | [http://localhost:8009/](http://localhost:8009/)               |
| Kitodo ContentServer (`images`) | [http://localhost:8010/images/](http://localhost:8009/images/) |

**Note:** Make sure you pre build the containers first! See section '[Using `docker-compose`](./docs/docker-compose.md#docker-compose)'.

<a name="env"></a>
List of environment variables
=============================

## Defined by files in this repository

This table contains all environment variables, it's default value and it's usage. This doesn't include variables
defined by containers that are extended or those that are only used in scripts.

| Name                       | Default value                                                                                          | Description                                                                           | `data` | `images` | `search` | `web` | Scripts                              |
|----------------------------|--------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|--------|----------|----------|-------|--------------------------------------|
| BUILD_CONTEXT              | /mnt/build-context                                                                                     | Directory where the build directory is mounted to                                     | ✗      | ✗        | ✗        | ✗     | -                                    |
| BUILD_DIR                  | /tmp/archaeo18                                                                                         | Directory for data transformations (temporary)                                        | ✗      |          |          |       | -                                    |
| REQ_BUILD                  | _differs for each container_                                                                           | Lists of packages to install (required during build)                                  | ✗      | ✗        | ✗        | ✗     | `generate-data.sh`                   |
| DATADIR                    | /data                                                                                                  | Directory (volume) to store the data files                                            | ✗      |          | ✗        | ✗     | `populate-index.sh`                  |
| ARCHAEO18_SCRIPTS          | /opt/archaeo18                                                                                         | Location of project specific scripts                                                  | ✗      |          | ✗        | ✗     | `populate-index.sh`                  |
| WATCH_FILE                 | .data-created                                                                                          | File to indicate data has ben created                                                 | ✗      |          | ✗        |       | -                                    |
| NGINX_CACHE                | /var/cache/nginx                                                                                       | Nginx cache directory                                                                 |        |          |          | ✗     | -                                    |
| NGINX_USER                 | root                                                                                                   | The user Nginx runs as                                                                |        |          |          | ✗     | `setup-nginx.sh`                     |
| PROXY_CACHE                | /cache                                                                                                 | Root of the `cache` volume (proxy in production mode)                                 |        |          |          | ✗     | -                                    |
| DEPLOY_HOST_NAME           | localhost:8008                                                                                         | The host name where the containers will be deployed (set by `HOST_NAME` during build) |        |          |          | ✗     | `setup-nginx.sh`                     |
| SOLR_CORE                  | archaeo18                                                                                              | Name of the Solr core to be created and used                                          |        |          | ✗        |       | `populate-index.sh`                  |
| SOLR_ROOT                  | /opt/solr                                                                                              | Solr root directory                                                                   |        |          | ✗        |       | -                                    |
| SOLR_HOME                  | /opt/solr/server/solr                                                                                  | Sorl home directory                                                                   |        |          | ✗        |       | -                                    |
| SOLR_PORT                  | 8983                                                                                                   | The port Solr will listen on                                                          |        |          | ✗        |       | `populate-index.sh`                  |
| ARCHAEO18_XSLT             | /opt/archaeo18/xslt                                                                                    | Directory project specific XSLT files                                                 |        |          | ✗        |       | `populate-index.sh`                  |
| SOLR_DATA_DIR              | /index                                                                                                 | Directory where the index itself is stored                                            |        |          | ✗        |       | `populate-index.sh`                  |
| SAXON_DIR                  | /opt/saxon                                                                                             | Saxon installation directory                                                          |        |          | ✗        |       | `populate-index.sh`                  |
| VERBOSE                    | false                                                                                                  | Be verbose to debug scripts                                                           |        |          | ✗        |       | -                                    |
| GIT_URL                    | https://github.com/kitodo/kitodo-contentserver.git                                                     | Git URL of the Kitodo ContentServer                                                   |        | ✗        |          |       | -                                    |
| IMAGE_URL                  | https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18-images/-/archive/master/archaeo18-images-master.zip | URL of the image file archive                                                         |        | ✗        |          |       | -                                    |
| IMAGE_DIR                  | /images                                                                                                | Final image directory (volume)                                                        |        | ✗        |          |       | -                                    |
| JETTY_SERVLET_API          | /lib/servlet-api-3.1.jar                                                                               | Path componet of the Servlet API                                                      |        | ✗        |          |       | -                                    |
| JETTY_DEPLOY_DIR           | /var/lib/jetty/webapps                                                                                 | Directory to deploy web apps to                                                       |        | ✗        |          |       | -                                    |
| CS_DIR                     | /contentServer                                                                                         | Path component where to bild the ContentServer                                        |        | ✗        |          |       | -                                    |
| CS_WAR                     | /dist/kitodo-contentserver-3.0.0.war                                                                   | Path component of the resulting War file                                              |        | ✗        |          |       | -                                    |
| REWRITE_URL                | http://central.maven.org/maven2/org/tuckey/urlrewritefilter/4.0.3/urlrewritefilter-4.0.3.jar           | URL of the URLRewriteFilter library                                                   |        | ✗        |          |       | -                                    |
| WORKDIR                    | _differs for each container_                                                                           | Working directory for containers and scripts                                          |        |          |          | ✗     | `change-host.sh`, `generate-data.sh` |
| NEW_HOST_NAME_FILE         | .new-host-name                                                                                         | Name of the file to store the new host in after sources had been changed              |        |          |          |       | `change-host.sh`                     |
| IMAGE_SCHEME               | http://                                                                                                | The scheme to use when servin images (including '://')                                |        |          |          | ✗     | `change-scheme.sh`                   |

Environment variables are defined in the Dockerfiles where they are used, additionally a `.env` file is provided 
to override those settings. This way it's guaranteed, that the settings don't differ between those files when build together 
and that they can still be build independently. 

All these settings can be controlled via `.env` file.

## Defined by containers

This variables are defined elsewere and used by the build system directly, implicit used variables aren't listed.

| Name                       | Defined by                         | Used by                  |
|----------------------------|------------------------------------|--------------------------|
| SOLR_VERSION               | solr Dockerfile                    | `update-solr.sh`         |
| SOLR_USER                  | solr Dockerfile                    | `search` Dockerfile      | 

<a name="notes"></a>
Notes
=====

## Initialize git sub modules

To initialize Git submodules you can run the following:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git submodule update --init --recursive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Updating Solr (to version 8 or higher)

The internal structure of the Solr Docker image has [changed](https://github.com/docker-solr/docker-solr#updating-from-docker-solr5-7-to-8).
The current `Dockerfile` doesn't throw an error when build with the `solr:8-slim` base image, but it hasn't been tested yet.

## Docker BuildKit messages

To get every output of `docker build` commands append the following to the `build` command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--progress=plain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

<a name="note_compose"></a>
## Composer Timeout

One can extend the timeout for composer using a environment variable:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPOSER_PROCESS_TIMEOUT=2000
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### Building a reduced size production image of the `data` container

It's possible to reduce the size of the image by building a special reduced size version.
This step is not needed anymore!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -f ./docker/data/Dockerfile.prod .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
