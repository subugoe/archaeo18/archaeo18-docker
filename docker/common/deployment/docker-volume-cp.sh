#!/bin/sh

# Copy Docker volumes

# This script is inspired by https://github.com/moby/moby/issues/31154#issuecomment-360635624

#Check if `docker` command exits
if [ -x "$(command -v docker)" ] ; then
	DOCKER="$(command -v docker)"
elif [ -x "$DOCKER" ] ; then
	DOCKER="$DOCKER"
else
	echo "'docker' command not found, exiting!"
	exit 2
fi

# Check for param 1, the source docker volume
if [ -z "$1" ] ; then
    echo "No source docker volume given!"
    exit 1
else
	if docker volume inspect $1 &> /dev/null ; then
    	echo "Using '$1' as source docker volume"
    	SOURCE="$1"
    else
    	echo "Docker volume '$1' doesn't exist!"
    	exit 3
    fi
fi

# Check for param 2, the target docker volume 
if [ -z "$2" ] ; then
    echo "No target docker volume given!"
    exit 2
else
    if docker volume inspect $2 &> /dev/null ; then
    	echo "Docker volume '$2' already exist!"
    	exit 4
    else
		echo "Using '$2' as target docker volume"
		TARGET="$2"
	fi
fi

# Start the copy process
echo "Creating new docker volume '$TARGET'"
docker volume create --name $TARGET > /dev/null
echo "Copying contents of '$SOURCE' to '$TARGET'"
docker run --rm -it -v $SOURCE:/from -v $TARGET:/to alpine sh -c "cd /from ; cp -av . /to"
if [ $? -ne 0 ] ; then
	echo "Something went wrong, exiting!"
	exit 5
fi