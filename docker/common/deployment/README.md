Notes on deployment
===================

To get rid of `UnixHTTPConnectionPool(host='localhost', port=None): Read timed out. (read timeout=60)`

```
export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120
```
