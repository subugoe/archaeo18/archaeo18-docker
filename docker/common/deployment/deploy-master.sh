#!/bin/sh

# Variables
DOCKER_COMPOSE_BASE='docker-compose.yml'
WEB_PORT=$(grep 80 $DOCKER_COMPOSE_BASE | cut -d "'" -f 2 |cut -d: -f1)

# Make sure we see the calls in the GitLab output
set +x

# Pull new versions of images, including our own
docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
docker pull docker.gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker/web-maintenance:master

# Try some simple black magic here to keep the downtime low

# Parse docker-compose.yml files for used images, pull them, clone them by adding additional tags based on the image names
IMAGE_SUFFIX="-init"
DOCKER_COMPOSE_SOURCE=$(docker-compose -f $DOCKER_COMPOSE_BASE -f docker-compose.prod.yml config)
SOURCE_IMAGES=$(echo $DOCKER_COMPOSE_SOURCE | grep "image:" | sed 's/ *image: \(.*\)/\1/g')
# TODO: Get the volumes
#SOURCE_VOLUMES=$(echo $DOCKER_COMPOSE_SOURCE | grep "volumes:" | sed 's/ *image: \(.*\)/\1/g')
for IMAGE in $SOURCE_IMAGES ; do
	echo "Pulling '$IMAGE' and tagging it as '$IMAGE$IMAGE_SUFFIX'"
	docker pull "$IMAGE"
	docker tag "$IMAGE" "$IMAGE$IMAGE_SUFFIX"
done

# Create or use some mocked docker-compose.yml file to start the required clones, but changed open ports and with other volumes
DOCKER_COMPOSE_TARGET=DOCKER_COMPOSE_SOURCE
# Rewrite image names
for IMAGE in $SOURCE_IMAGES ; do
	DOCKER_COMPOSE_TARGET=$(echo DOCKER_COMPOSE_TARGET | sed 's/$IMAGE/$IMAGE$IMAGE_SUFFIX/g')
do
# Rewrite volume names
# Rewrite ports

# Start the containers to trigger init scripts, ignore .env file for this
# COMPOSE_SKIP_ENV_FILE=1
echo DOCKER_COMPOSE_TARGET | docker-compose -f - up -d

# If everything has been started (checked by some HTTP monitoring requests), tear it down again.

# Shut down the running instance, remove the volumes while at it
# Use '--remove-orphans' to get rid of containers that can exist if started with the wrong 'docker-compose.yml' file(s)
docker-compose -f $DOCKER_COMPOSE_BASE -f docker-compose.prod.yml down -v --remove-orphans

# Start the maintanance message
TEMP_CONTAINER=$(docker run -d -p $WEB_PORT:80 docker.gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker/web-maintenance:master)

# Rename (copy) the initialized docker volumes 

# Stop tem container and start everything again
docker rm -f $TEMP_CONTAINER
docker-compose -f $DOCKER_COMPOSE_BASE -f docker-compose.prod.yml up -d