#!/bin/sh

# Read common variables
source $(dirname "$0")/config.sh

# Files to exclude in replace
BLACKLIST="node_modules test tests ropen-backend bower_components unit_testing testdata build examples *.gif *.jpg *.png *.psd *.pdf *.ico *.ttf *.eps *.jar *.wav *.woff"

# Internal state vars
CHANGE=true
ADDITIONAL_SCRIPT=""

# Check arguments
# Check for param 1, the diretory to operate on
if [ -z "$1" ] ; then
    echo "No working directory given, using '$WORKING_DIR'"
else
    echo "Using $1 as working directory"
    WORKING_DIR="$1"
fi

# Check for param 2, the new host name
if [ -n "$2" ] ; then
    echo "Using '$2' as new host name"
    NEW_HOST_NAME="$2"
elif [ -n "$DEPLOY_HOST_NAME" ] ; then
    echo "Using '$DEPLOY_HOST_NAME' (from ENV) as new host name"
    NEW_HOST_NAME="$DEPLOY_HOST_NAME"
else
	echo "No new host name given, using '$NEW_HOST_NAME'"
fi

# Check for param 3 , additional scripts for changing things
if [ -z "$3" ] ; then
    echo "No third parameter given, not running additional change command"
else
	ADDITIONAL_SCRIPT="$3"
	echo "Running additional change command(s): '$ADDITIONAL_SCRIPT'"
fi

# Check for param 4 the programm to run
if [ -z "$4" ] ; then
    echo "No forth parameter given, not running anything"
	RUN=false
else
	RUN=true
fi

# Debugging
#if [[ "$VERBOSE" == "true" ]] ; then
#	set -x
#fi

# Check if host name has already been changed
if [ -r "$WORKING_DIR/$NEW_HOST_NAME_FILE" ] ; then
    if grep -q "$NEW_HOST_NAME" "$WORKING_DIR/$NEW_HOST_NAME_FILE" ; then
  		if grep -q "://" "$WORKING_DIR/$NEW_HOST_NAME_FILE" ; then
  			echo "Scheme found in status file, but no host name change requested, doing a full rebuild to trigger additional scripts"
# TODO: If just the scheme changes but not the host name, this currently does more then expected
  		else
  			echo "Directory $WORKING_DIR has already been changed to $NEW_HOST_NAME"
  			CHANGE=false
  		fi
	else
		HOST_NAMES="$(cat $WORKING_DIR/$NEW_HOST_NAME_FILE)"
		echo "Status file has been found, but new host name is different ($HOST_NAMES)."
		echo "Trying to recover, but it  might be better to start from a clean image again - don't forget to prune the volumes!"
# Check for double slash to extract the old host name
		if test -n "$(echo "$HOST_NAMES" | $GREP '//')" ; then
			HOST_NAMES=$(echo "$HOST_NAMES" | sed 's/^.*:\/\/\(.*\)$/\1/')
			echo "Host name file contains scheme - host name was $HOST_NAMES"
		else
		    echo "No scheme found in host name file"
		fi
	fi
else
	echo "No status file found"
fi

if [[ "$CHANGE" == "true" ]] ; then
# Write the host names out to mak transparent what will be done
	echo "The following patterns will be used, this doesn't change anything yet"
	for PATTERN in $(echo "$HOST_NAMES" | tr ' ' '\n'); do 
		echo "Looking for $PATTERN ('$GREP -r \"$PATTERN\" $WORKING_DIR')"
	done

	echo "Processing blacklist"
	FIND_CMD="$FIND -L $WORKING_DIR -type f "
	for ENTRY in $BLACKLIST ; do
		FIND_CMD="$FIND_CMD -not "
		if test -n "$(echo "$ENTRY" | $GREP '*.')" ; then
			FIND_CMD="$FIND_CMD -name $ENTRY"
		else
			FIND_CMD="$FIND_CMD -path */$ENTRY/*"
		fi
		FIND_CMD="$FIND_CMD "
	done

	if [[ "$VERBOSE" == "true" ]] ; then
		echo "Blacklist is '$FIND_CMD'"
	fi

	echo "Generating file list..."
	# Loop through the files and directories
	set -o noglob
	# The suffix 'grep' is needed to exclude binary files like images or PDFs
	FILES=`$FIND_CMD -exec $GREP -Iq . {} \; -print`
	set +o noglob
	echo "File list generated, searching and replacing..."
	for FILE in $FILES; do
			if [ ! -r "$FILE" ] ; then
				if [[ "$VERBOSE" == "true" ]] ; then
					echo "Warning: Couldn't read $FILE"
				fi
				continue
			fi
			if [[ "$VERBOSE" == "true" ]] ; then
				echo "Processing $FILE:"
			fi
	# Loop through the host names
			for PATTERN in $(echo "$HOST_NAMES" | tr ' ' '\n'); do 
				SED_PATTERN=$(echo "$PATTERN" | sed 's/\//\\\//g')
				if [[ "$VERBOSE" == "true" ]] ; then
					echo -e "\tPattern: '$PATTERN', escaped: '$SED_PATTERN'"
				fi
				if $GREP -q "$SED_PATTERN" "$FILE" ; then
					echo -e "\tProcessing $FILE for $PATTERN (to be replaced with $NEW_HOST_NAME)" 
					$SED -i "s#$SED_PATTERN#$NEW_HOST_NAME#g" "$FILE"		
				fi
			done
	done

	echo "Writing status file $WORKING_DIR/$NEW_HOST_NAME_FILE"
	echo "$NEW_HOST_NAME" > "$WORKING_DIR/$NEW_HOST_NAME_FILE"
	
	if [ -n "$ADDITIONAL_SCRIPT" ] ; then
		echo "Running '$ADDITIONAL_SCRIPT'"
		if echo "$ADDITIONAL_SCRIPT" | grep -E '[ "]' >/dev/null ; then
  			echo "Command contains white space"
		fi
		
		MSG=$(eval "$ADDITIONAL_SCRIPT")
		ERROR=$?
		if [ $ERROR -ne 0 ] ; then
			echo "Got error: $ERROR - $(echo "$MSG" | tail -n 2)"
			exit $ERROR + 100
		else 
			echo "Ran '$ADDITIONAL_SCRIPT' without errors:"
			echo "$MSG"
		fi
	fi

else
	echo "Files already changed"
fi

if [[ "$RUN" == "true" ]] ; then
	shift
	shift
	shift
	echo "Starting $@"
	exec "$@"
fi