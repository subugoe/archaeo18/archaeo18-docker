#!/bin/sh

# This is a space separated list of host names (including ports) to replace 
HOST_NAMES="134.76.21.92:8080 134.76.21.92 heyne-digital.tc.sub.uni-goettingen.de"

# Defaults
NEW_HOST_NAME="heyne-digital.de"
WORKING_DIR="."
NEW_HOST_NAME_FILE=.new-host-name

# Commands (defaults)
GREP="/bin/grep"
FIND="/usr/bin/find"
SED="/bin/sed"

# Other Settings
VERBOSE=false

# For debugging and development on OS X 
if [[ "$OSTYPE" == "darwin"* ]]; then
	echo "Mac OS X detected using GNU tool from Mac Ports"
	GREP="/opt/local/bin/ggrep"
	FIND="/opt/local/bin/gfind"
	SED="/opt/local/bin/gsed"
else
	GREP=$(which grep)
	FIND=$(which find)
	SED=$(which sed)
fi

echo "The following programs will be used"
echo "Using 'grep' at $GREP"
echo "Using 'find' at $FIND"
echo "Using 'sed' at $SED"
