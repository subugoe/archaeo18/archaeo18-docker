#!/bin/sh

# Change the scheme prefix of links
# This need to operate on the following files
#  - source/content/app
#    - list-docs.xml
#    - map/map.kml
#    - mets/*
#

# Read common variables
source $(dirname "$0")/config.sh

#WHITE_LIST="*.xml *.kml *.mets mets list-docs.xml map/map.kml"
WHITE_LIST="*/public/content/app/mets/*.xml */public/content/app/list-docs.xml */public/content/app/map/map.kml"
WORKING_DIR_SUFFIX=""
OLD_SCHEME='http://'
NEW_SCHEME='http://'

# Check arguments
# Check for param 1, the diretory to operate on
if [ -z "$1" ] ; then
    echo "No working directory given, using '$WORKING_DIR'"
else
    echo "Using $1 as working directory"
    WORKING_DIR="$1"
fi

# Check for param 2, the new scheme
if [ -n "$2" ] ; then
    echo "Using '$2' as new scheme"
    NEW_SCHEME="$2"
elif [ -n "$IMAGE_SCHEME" ] ; then
    echo "Using '$IMAGE_SCHEME' (from ENV) as new host name"
    NEW_SCHEME="$IMAGE_SCHEME"
else
	echo "No new scheme given, using '$NEW_SCHEME'"
fi

# Check if host name has already been changed
if [ -r "$WORKING_DIR/$NEW_HOST_NAME_FILE" ] ; then
	HOST_NAME="$(cat $WORKING_DIR/$NEW_HOST_NAME_FILE)"
	echo "Status file has been found, searching for '$HOST_NAME'."
# Check for double slash to extract the old scheme
	if test -n "$(echo "$HOST_NAME" | $GREP '//')" ; then
		echo "Host name file contains scheme"
		OLD_SCHEME=$(echo "$HOST_NAME" | sed 's/^\(.*\):\/\/.*$/\1/')"://"
	fi
else
	echo "No status file found"
	exit 1
fi

if test "$OLD_SCHEME" == "$NEW_SCHEME" ; then
	echo "No change needed, exiting."
	exit 0
fi

# We need tio create a call like this to find files to look into.
# find . \(  -path '*/source/content/app/mets/*.xml' -o -path '*/source/content/app/map/*.kml' -o -path '*/source/content/app/list-docs.xml' \)   -print

echo "Processing whitelist"
FIND_CMD="$FIND -L $WORKING_DIR -type f ("
for ENTRY in $WHITE_LIST ; do
	FIND_CMD="$FIND_CMD -path $ENTRY -o "
done

# Cut off the last '-o'
FIND_CMD=$(echo "$FIND_CMD" | awk '{print substr( $0, 0, length() - 3 )}')" )"

if [[ "$VERBOSE" == "true" ]] ; then
	echo "Whitelist is '$FIND_CMD'"
fi

# Generate the expressions to look for / to replace
SEARCH="$OLD_SCHEME$HOST_NAME"
REPLACEMENT="$NEW_SCHEME$HOST_NAME"

echo "Generating expression for string '$SEARCH' to replace it with '$REPLACEMENT'"

echo "Generating file list..."
# Loop through the files and directories
set -o noglob
FILES=`$FIND_CMD -print`
set +o noglob
echo "File list generated, searching and replacing..."
for FILE in $FILES; do
		if [ ! -r "$FILE" ] ; then
			echo "Warning: Couldn't read $FILE"
			continue
		fi
		if [[ "$VERBOSE" == "true" ]] ; then
			echo "Processing $FILE:"
		fi

		if $GREP -q "$SEARCH" "$FILE" ; then
			SED_SEARCH=$(echo "$SEARCH" | sed 's/\//\\\//g')
			SED_REPLACEMENT=$(echo "$REPLACEMENT" | sed 's/\//\\\//g')
			echo -e "\tProcessing $FILE for '$SED_SEARCH' (to be replaced with '$SED_REPLACEMENT')" 
			$SED -i "s#$SED_SEARCH#$SED_REPLACEMENT#g" "$FILE"		
		fi
done

echo "$NEW_SCHEME$HOST_NAME" > "$WORKING_DIR/$NEW_HOST_NAME_FILE"