#!/bin/sh

case "$SOLR_VERSION" in
    5*)  echo "Solr version is $SOLR_VERSION, nothing to change" ;; 
    6*) echo "Updating for Solr 6.x";
        sed -i "s/<luceneMatchVersion>LUCENE_42<\/luceneMatchVersion>/<luceneMatchVersion>6.4.3<\/luceneMatchVersion>/g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ;
        sed -i "s/<requestHandler name=\"\/admin\/\" class=\"org.apache.solr.handler.admin.AdminHandlers\" \/>//g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ; 
        sed -i "s/enablePositionIncrements=\"true\"//g" $SOLR_HOME/$SOLR_CORE/conf/schema.xml ;;
    7*) echo "Updating for Solr 7.x";
        sed -i "s/<luceneMatchVersion>LUCENE_42<\/luceneMatchVersion>/<luceneMatchVersion>7.8.0<\/luceneMatchVersion>/g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ;
        sed -i "s/<requestHandler name=\"\/admin\/\" class=\"org.apache.solr.handler.admin.AdminHandlers\" \/>//g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ; 
        sed -i "s/enablePositionIncrements=\"true\"//g" $SOLR_HOME/$SOLR_CORE/conf/schema.xml ;;
    8*) echo "Updating for Solr 8.x, this hasn't ben tested yet!";
        sed -i "s/<luceneMatchVersion>LUCENE_42<\/luceneMatchVersion>/<luceneMatchVersion>8.2.1<\/luceneMatchVersion>/g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ;
        sed -i "s/<requestHandler name=\"\/admin\/\" class=\"org.apache.solr.handler.admin.AdminHandlers\" \/>//g" $SOLR_HOME/$SOLR_CORE/conf/solrconfig.xml ; 
        sed -i "s/enablePositionIncrements=\"true\"//g" $SOLR_HOME/$SOLR_CORE/conf/schema.xml ;;
    *)  echo "Solr version is $SOLR_VERSION, not supported" ;
# Fail if upgrade isn't implemented
        exit 1 ;; 
esac
