#!/bin/bash

WATCH_FILE=.data-created

SAXON_CMD="java -jar $SAXON_DIR/saxon9he.jar"
SOLR_XSL=/opt/archaeo18/xslt/solr.xsl
IMPORT_DIR=/data/tei-enriched/
POST_SH=$ARCHAEO18_SCRIPTS/post.sh
CONTENT_ARCHIVE=$DATADIR/content.zip

# SOLR_DATA_DIR is set by the container
# ARCHAEO18_SCRIPTS is set by the container
# ARCHAEO18_XSLT is set by the container
# BUILD_DIR is set by the container
# SAXON_DIR is set by the container
# SOLR_PORT is set by the container
# SOLR_CORE is set by the container
# DATADIR is set by the container

# Internal state vars
INDEX=true

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

#Check for param 1  the programm to run
if [ -z "$1" ]; then
    echo "No parameter given, not running anything"
	RUN=false
else
	RUN=true
fi

# Check if data might be present
if [[ -d "$DATADIR/" ]] && [[ -r "$DATADIR/" ]] ; then
    echo "$DATADIR/ exists and is readable"
else
	echo "$DATADIR/ either doesn't exist or isn't readable, exiting"
	exit 1
fi

# Check if import files exist
if [[ -d "$IMPORT_DIR" ]] && [[ -r "$IMPORT_DIR" ]] ; then
	echo "$IMPORT_DIR exists and is readable"
elif [ -r $CONTENT_ARCHIVE ] ; then  
	echo "$IMPORT_DIR not found, try to create it!"
	mkdir -p $IMPORT_DIR
	busybox unzip $CONTENT_ARCHIVE 'app/tei-enriched/*' -d $IMPORT_DIR
	echo "Import files extracted"
else 
	echo "Import files not found, exiting!"
	exit 3
fi

# Check if important variables are set
if [ -z "$SOLR_DATA_DIR" ] ; then
    echo "SOLR_DATA_DIR isn't set, exiting"
    exit 2
fi

# TODO: run arguments, even if index exist
# Check if index is already populated
if [ `du -s $SOLR_DATA_DIR | cut -f 1` -gt 16 ] ; then 
    echo "Index seems to be populated already, exiting"
    INDEX=false
fi

# The indexing part
if [[ "$INDEX" == "true" ]] ; then

	# TODO: Check if we need to wait for Solr
	# Check for the data
	until [ -f "$DATADIR/$WATCH_FILE" ] ; do
		 echo "File $DATADIR/$WATCH_FILE not found yet, waiting (Make sure you have build the data container first!)..."
		 sleep 5
	done
	echo "File found, starting Solr import"

	# This follows the guide at https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18/blob/gh-pages/data/solr/README.md
	# Synonyms aren't created since they are part of the repository
	# TODO This needs to be changed if data is generated during the build

	echo "Create working directory in $BUILD_DIR"
	mkdir -p $BUILD_DIR/import

	echo "Starting Solr in background"
	start-local-solr

	echo "Looping over files to be transformed"
	for FILE in $IMPORT_DIR/* ; do
		NAME=`basename $FILE`
		echo "Processing $FILE to $BUILD_DIR/import/$NAME.xml"  
		$SAXON_CMD -s:$FILE -xsl:$SOLR_XSL -o:$BUILD_DIR/import/$NAME.html
	done

	echo "Importing from $BUILD_DIR/import/"
	#cd `basename $POST_SH`
	cd $ARCHAEO18_SCRIPTS
	bash $POST_SH $BUILD_DIR/import/*.html

	echo "Optimizing index $SOLR_CORE"
	curl http://localhost:$SOLR_PORT/solr/$SOLR_CORE/update?optimize=true

	echo "Import completed!"

	echo "Stoping Solr"
	stop-local-solr

	echo "Removing working directory in $BUILD_DIR"
	rm -r $BUILD_DIR/import
else
	echo "Index already exist, delete it from disk to trigger a reindex"
fi

if [[ "$RUN" == "true" ]] ; then
	echo "Starting $@"
	exec "$@"
fi