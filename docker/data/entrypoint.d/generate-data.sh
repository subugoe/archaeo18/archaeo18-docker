#!/bin/sh

# WORKDIR=/data is set by the Dockerfile env
# BUILD_DIR=/data is set by the Dockerfile env

REQ_BUILD="openjdk8 apache-ant"

echo "Warning! This script is not tested yet!"
echo "Installing dependencies... "

# check if we are running as root
if [[ $EUID -ne 0 ]]; then
    echo "Not running as root, dependencies will not be installed, this can fail in later stages"
else 
	echo "Installing dependencies"
# Install dependencies
	apk --update add --no-cache $REQ_BUILD
fi

if [ ! -r "$BUILD_DIR.tar.bz2" ] ; then
    echo "Data archive missing, exiting!"
    exit 2
else
	echo "Extracting $BUILD_DIR.tar.bz2"
	cd "$BUILD_DIR/../"
	busybox tar xjf "$BUILD_DIR.tar.bz2" -C /
fi

echo "Cleaning up preexisting data"
rm -rf $WORKDIR/*

echo "Building data files (Warning this isn't tested, expect errors!)"
cd $BUILD_DIR 
ant -f build.xml data.generate

echo "Data files have been generated, make sure you recreate the list of synonyms as described in https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18/blob/gh-pages/data/solr/README.md"

mv /tmp/archaeo18/content $WORKDIR/
rm -rf "$BUILD_DIR"
touch $WORKDIR/$WATCH_FILE

# Remove dependencies (if root)

if [[ $EUID -eq 0 ]]; then
    echo "Removing dependencies"
	apk del $REQ_BUILD
fi

echo "You might want to remove $BUILD_DIR"
