#!/bin/sh

# Defaults
NEW_HOST_NAME="heyne-digital.de"
# This is the file to be used as a template and to be changed via parameter two.
NGINX_CONF_NEW="/etc/nginx/conf.d/default.conf"
# This is the destination location of the Nginx configuration, this shouldn't be changed.
NGINX_CONF="/etc/nginx/conf.d/default.conf"
# NGINX_USER is set by the container
# DEPLOY_HOST_NAME is set by container

# Check for param 1, the new host name
if [ -n "$1" ] ; then
    echo -e "New host name given, using '$1'"
    NEW_HOST_NAME="$1"
elif [ -n "$DEPLOY_HOST_NAME" ] ; then 
    echo -e "New host name given via ENV, using '$DEPLOY_HOST_NAME'"
    NEW_HOST_NAME="$DEPLOY_HOST_NAME"
else
    echo -e "No new host name given, using '$NEW_HOST_NAME'"
    NEW_HOST_NAME="$NEW_HOST_NAME"
fi

# Check for param 2, the configuration file to use
if [ -z "$2" ] ; then
	echo "No configuration file to use given, using '$NGINX_CONF_NEW'"
else
	echo "Using $NGINX_CONF_NEW as configuration file"
	NGINX_CONF_NEW="$2"
fi

# Check if we have the rights to do some changes, if not, we should fail here
if [ -w $NGIX_CONF ] ; then
	echo "Nginx configuration is writeable, proceding"
else
	echo "Nginx configuration is not writeable, either due to access rights (running as $NGINX_USER) or because it's mounted readonly"
	exit 1
fi

# Check for Nginx default configuration
if cat /etc/nginx/conf.d/default.conf | tr -d '\n' | grep -q "location / {\s*root   /usr/share/nginx/html;" - ; then
	echo "Warning: Nginx default configuration seem to be in place!"
fi

# If the Nginx given configuration and the existing are the same, change the existing,
# otherwise use the different as a template to create a new one.
if [ $NGINX_CONF_NEW != $NGINX_CONF ] ; then
	echo "Coping '$NGINX_CONF_NEW' to '$NGINX_CONF'"
	cp $NGINX_CONF_NEW $NGINX_CONF
else
	echo "Configurations are identical, just changing the existing"
fi

# Replace hostname
# TMPFILE is needed since we can't create a tmp file in '/etc/nginx/' as non-root user
TMPFILE=$(mktemp -t $(basename $0)XXXXXX)
sed "s/server_name heyne-digital.local;/server_name NEW_HOST_NAME;/g" $NGINX_CONF > $TMPFILE
mv $TMPFILE $NGINX_CONF
# This can fail if File is mounted by docker, catch it.
if [ $? -ne 0 ] ; then
	echo -e "\nCouldn't change configurations, error - $NGINX_CONF might be mounted by Docker!"
	rm -f $TMPFILE
	exit 2
fi
rm -f $TMPFILE
if [ "$NGINX_USER" == "nginx" ] ; then
# Replace username if not `root`
# Does this needs to be run as root?
# TMPFILE is needed since we can't create a tmp file in '/etc/nginx/' as non-root user
	TMPFILE=$(mktemp -t $(basename $0)XXXXXX)
	echo "Changing Nginx user to '$NGINX_USER'"
	sed "s/user root;/user $NGINX_USER;/g" /etc/nginx/nginx.conf > $TMPFILE
	mv $TMPFILE /etc/nginx/nginx.conf
# This can fail if File is mounted by docker, catch it.
	if [ $? -ne 0 ] ; then
		echo -e "\nCouldn't change configurations, error - probably permission related!"
		rm -f $TMPFILE
		exit 2
	fi
	rm -f $TMPFILE
elif [ "$NGINX_USER" == "root" ] ; then
    echo "NGINX_USER is root, nothing to change"
else
    echo "Unknown user $NGINX_USER, exiting!"
    exit 1
fi

