upstream docker-images {
	server images:8080;
}

upstream docker-search {
	server search:8983;
}

proxy_cache_path /cache levels=1:2 keys_zone=archaeo18_cache:2m max_size=10g 
                 inactive=14d use_temp_path=off;

server {
    index index.html;
    server_name heyne-digital.local;
    error_log  /var/log/nginx/error.log;
    disable_symlinks off;

    access_log off;
    root /var/www/html/public;

    location / {
        # try to serve file directly, fallback to index.php
        # try_files $uri /index.php$is_args$args;
        disable_symlinks off;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }
    
    location /images {
            return 302 http://$http_host/images/;
    }
    
    location /images/ {
        proxy_cache archaeo18_cache;
        proxy_cache_use_stale error timeout http_500 http_502 http_503 http_504;
        proxy_cache_methods GET HEAD POST;
    	proxy_pass http://docker-images;
		proxy_set_header Host $http_host;
    	proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}
	
	location /solr/ {
	    proxy_cache archaeo18_cache;
	    proxy_cache_use_stale error timeout http_500 http_502 http_503 http_504;
	    proxy_cache_methods GET HEAD;
    	proxy_pass http://docker-search;
		proxy_set_header Host $http_host;
    	proxy_set_header X-Forwarded-Host $host:$server_port;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}
	
	location ~ ^/(content/app)/(.*)$ {
		unzip on;
    	unzip_archive "$document_root/content.zip";
    	unzip_path "$2";
    	unzip_autoindex off;
    	unzip_nocase fallback;
    	unzip_path_encoding "UTF-8";
	}
}
