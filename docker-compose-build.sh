#!/bin/bash

PROJECT_NAME=`basename $PWD`
DOCKER_DIR=./docker
BLACKLIST="app common solr8"

NEW_HOST_NAME="localhost:8008"
# We use 'export' here and below to work in OS X as well
export DEFAULT_HOST_NAME="localhost:8008"

# Settings
EXPERIMENTAL=false
SEPARATOR_CHAR=_
PREFIX=""

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RESET='\033[0m'

#Check if `docker` command exits
if [ -x "$(command -v docker)" ] ; then
	DOCKER="$(command -v docker)"
elif [ -x "$DOCKER" ] ; then
	DOCKER="$DOCKER"
else
	echo -e "${RED}'docker' command not found, exiting!${RESET}"
	exit 2
fi

# Check for param 1, the new host name
if [ -z "${1}" ] ; then
    echo -e "${YELLOW}No new host name given, using '${NEW_HOST_NAME}'${RESET}"
else
    echo -e "${YELLOW}Using ${1} as new host name${RESET}"
    NEW_HOST_NAME="${1}"
fi

export NEW_HOST_NAME

# Check for param 2 , additional BUILD_ARGS
if [ -z "${2}" ] ; then
    echo -e "${YELLOW}No build args given${RESET}"
else
	shift
	BUILD_ARGS="$@"
fi

# Check if external $IMAGE_SEPERATOR is set
if [ -x "${IMAGE_SEPERATOR}" ] ; then
	SEPARATOR_CHAR="${IMAGE_SEPERATOR}"
fi
echo -e "${YELLOW}Name of the image will use '${SEPARATOR_CHAR}' as separator${RESET}"

# Check if external $IMAGE_PREFIX is set
if [ -x "${IMAGE_PREFIX}" ] ; then
	PREFIX="${IMAGE_PREFIX}"
fi
echo -e "${YELLOW}Name of the image will use '${PREFIX}' as prefix${RESET}"


if [ "${EXPERIMENTAL}" == 'true' ] ; then
	echo -e "${YELLOW}Experimental features enabled${RESET}"
# Check if `git` command exits
	if [ -x "$(command -v git)" ] ; then
		GIT="$(command -v git)"
	elif [ -x "$GIT" ] ; then
		GIT="$GIT"
	else
		echo -e "${RED}'git' command not found, exiting!${RESET}"
		exit 3
	fi
	echo -e "${YELLOW}Using Git at '${GIT}'${RESET}"
# Get the name of the repository
	REMOTE_NAME=`basename $($GIT remote get-url origin) .git`
	echo -e "${YELLOW}Name of remote repository is '${REMOTE_NAME}'${RESET}"

# Check name of checkout directory
	for LINE in `grep -rioh 'FROM.*:latest' $DOCKER_DIR | cut -d ' ' -f 2 | tr '\n' ' '` ; do
		echo -e "${YELLOW}Checking '${LINE}'${RESET}"
		if [[ "${LINE}" =~ "${REMOTE_NAME}" ]] ; then
			echo -e "${GREEN}Names for repository directory name and 'FROM:' declarations in Dockerfiles match.${RESET}"
			break
		else
			echo -e "${YELLOW}Warning: Repository name doesn't match directory name for inherited builds, make sure you adjust your 'docker-compose.yml' files${RESET}"
			echo -e "${YELLOW}Replace '${REMOTE_NAME}' with '${PROJECT_NAME}'${RESET}"
			break
		fi
	done
fi

# Change .env file
if [ "${DEFAULT_HOST_NAME}" != "${NEW_HOST_NAME}" ] ; then
	if grep -q "${DEFAULT_HOST_NAME}" .env ; then
		echo -e "${YELLOW}Setting the new host in .env. Note, that this can fail if your sed doesn't know how to work with a empty extension.${RESET}"
		sed -i.bak "s/DEPLOY_HOST_NAME=${DEFAULT_HOST_NAME}/DEPLOY_HOST_NAME=${NEW_HOST_NAME}/g" .env
		echo -e "${GREEN}'.env' changed${RESET}"
		if grep -q ".env" .gitignore ; then
			rm -f .env.bak
			echo -e "${YELLOW}'.gitignore' already includes '.env'${RESET}"
		else
			echo -e ".env\n.env.bak" >> .gitignore
			echo -e "${YELLOW}Updated '.gitignore' to include '.env'${RESET}"
		fi
	else 
		echo -e "${YELLOW}'${DEFAULT_HOST_NAME}' not found in '.env' file, might has already been changed. Change it yourself.${RESET}"
	fi
	echo -e "${YELLOW}You need to adjust your docker-compose.yml file(s) yourself!${RESET}"
else
	echo -e "${GREEN}No change for '.env' needed${RESET}"
fi

BUILD_ARGS=" --build-arg HOST_NAME=$NEW_HOST_NAME $BUILD_ARGS"

# Check if docker directory exists
if [ -d "$DOCKER_DIR" ] ; then
    echo -e "${GREEN}${DOCKER_DIR} exists and is readable${RESET}"
else
	echo -e "${RED}${DOCKER_DIR} either doesn't exist or isn't readable - maybe not started in the right directory, exiting!${RESET}"
	exit 1
fi

echo "Building images"
for DIR in $DOCKER_DIR/*; do
    NAME=`basename $DIR`
    if [[ -d "${DIR}" ]] && [[ ! "${BLACKLIST}" =~ "${NAME}" ]] ; then
		echo "Processing ${DIR}"
		if [ -f docker/${NAME}/Dockerfile ] ; then 
			IMAGE_TAG="${PREFIX}${PROJECT_NAME}${SEPARATOR_CHAR}${NAME}"
			echo -e "${GREEN}Building '${DOCKER_DIR}/${NAME}/Dockerfile' (Tag: '${IMAGE_TAG}')${RESET}"
			DOCKER_BUILDKIT=1 $DOCKER build -t ${IMAGE_TAG} $BUILD_ARGS -f ${DOCKER_DIR}/${NAME}/Dockerfile --pull .
		else
			echo -e "${RED}'${DOCKER_DIR}/${NAME}/Dockerfile' not found!${RESET}"
		fi
		if [ "${EXPERIMENTAL}" == 'true' ] ; then
			echo -e "${YELLOW}Building experimental images for '${NAME}'${RESET}"
			if [ -f docker/${NAME}/Dockerfile.* ] ; then
				for FILE in docker/${NAME}/Dockerfile.* ; do
					SUFFIX="${FILE##*.}"
					IMAGE_TAG="${PREFIX}${PROJECT_NAME}${SEPARATOR_CHAR}${NAME}-${SUFFIX}"
					echo -e "${GREEN}Building '${DOCKER_DIR}/${NAME}/Dockerfile' (Tag: '${IMAGE_TAG}')${RESET}"
					DOCKER_BUILDKIT=1 $DOCKER build -t ${IMAGE_TAG} $BUILD_ARGS -f ${DOCKER_DIR}/${NAME}/Dockerfile.${SUFFIX} .
				done
			fi
		fi
    fi
done

echo -e "${GREEN}Everything has been build, make sure you run 'docker-compose up' with the '--no-build' flag${RESET}"
