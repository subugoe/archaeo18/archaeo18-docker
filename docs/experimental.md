<a name="status" />

Status
======

# Done / Working

-  Migrate repositories to GitLab ✓

    -  Add a note to existing repositories and deactivate them ✓

    -  Change URLs of `git` submodules ✓


-  Create Solr image and Solr core ✓

    -  Feed data into Solr using indexing script ✓


-  Create Nginx image ✓

    -  Configure `web` image as proxy for `images` and `search` ✓
  
    -  Change the server name for deployment ✓
  
    -  Regenerate or update METS files and image links using the new server name ✓

    -  Bundle Nginx default configuration

-  Find a way to get the images and add the to GitLab ✓

    - Create a image for the images ✓


-  Create the data image ✓

    -  Fix import errors ✗ - won't get fixed yet, since currently deployed data is available from the Git repository
  
    -  Use existing files for quicker deployment ✓


-  Reduce size of imagess ✓

    -  Use compression for raw TEI data for enrichment ✓

-  Document usage ✓

-  Test on Ubuntu ✓

# Additions
 
-  Use Nginx as content cache for images and search queries (in production mode) ✓

-  Update Solr (to Solr 7.x or above) ✓

-  Use GitLab to build [images](https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker/container_registry) ✓

# Experimental

-  Serve static files from Zip files using Nginx ([ngx_http_unzip_module.so](https://github.com/glabra/nginx-unzip-module))

# Known issues

-  If one changes the configuration via `Kitematic` and a container is restarted, the argument list (variable `$@`) seems to get dublicated. 

# TODO

-  Add automatic deployment

There are several additional TODO's in the sections below and within the files, run the following to find them:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
grep -Ri --exclude-dir=source --exclude-dir=.git --exclude='.*' 'todo' .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(This works with GNU `grep`, not with the BSD version - like on OS X)

# Out of scope / Won't changed

-  Make the data generation tasks work again. There are currently [several issues](https://gitlab.gwdg.de/subugoe/archaeo18/ropen-backend/issues/) as blockers, the `data` image contains the required tools.

-  Nginx

    -  Run Nginx as unprivileged user (already prepared)

<a name="experimental" />

Experimental features
=====================

## Serving files from zip files

The `prod` variant of the `web` image contains [ngx_http_unzip_module.so](https://github.com/glabra/nginx-unzip-module).
This module is configured by `site.prod.zip.conf`. It's currently untested and the needed zip file isn't generated during the build.
To enable the creation just remove the comment sign from the line `zip -9 -r content content` in the `Dockerfile` of the `data` image.
Afterwards there should be a working link in the `web` image from `var/www/html/public/content.zip` to `/data/content.zip`.

## Regenerating data files

To regenerate the data, the ['`generated-data.sh`](../README.md#generate-data) script can be used. But keep in mind that development of 
this script has ben abandoned since, fixing all possible errors was out of scope. The following issues might appear by the build 
system of this docker wrapper:

  - The mechanism to let Solr wait for data creation isn't tested.

  - If the mechanism for serving from zip files is in use, the access to generated data isn't guaranteed, since all scripts 
    aren't expecting files to be compressed.
    
  - For further enhancements like new documents the data creation tasks should be done in a new Docker image, since 
    from the current state some patches to the Salvador build system are required.

## Using pre build images

Using pre build images it is possible to have an easy way to do an automatic deployment.
The documentation on pre build images is part of the [`docker-compose` documentation](../docs/docker-compose.md#pre-build).