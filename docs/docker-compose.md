<a name="docker-compose"></a>

Using `docker-compose`
======================

Since the containers rely on the `Buildkit` Engine of Docker, `docker-compose` can't build them.

<a name="docker-compose-build.sh"></a>
# Build: `docker-compose-build.sh`

You need to build the containers first using `docker-compose-build.sh` first and the need to start them using `docker-compose` like so:
The script can also build some experimental containers if you set the variable `EXPERIMENTAL` in the script to `true`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh 
docker-compose up -d --no-build
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Note, that you still can use several `docker-compose.yml` files using the `-f` switch, see sections about 
development and production modes below.

The `docker-compose-build.sh` script also supports an argument to set the final host name for deployment:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh heyne-digital.de
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This lets the script pass the `--build-arg` `HOST_NAME` to each build process, only the `web` container uses it.

The script needs a bash shell to run.

## Arguments

| Position    | Default value      | Description                                                                |
|-------------|--------------------|----------------------------------------------------------------------------|
| 1           | localhost:8008     | Host name to run the web fronend on                                        |
| 2           |                    | Additional `--build-arg`                                                   |

**Note:** Setting the host name to the right value isn't really necessary, you can change it it later on by setting 
the `DEPLOY_HOST_NAME` env variable.

## Environment

`docker` needs to be on the path as well, otherwise it will fail. You can pass the location of the `docker` executable 
using the environment variable `DOCKER`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER=/snap/bin/docker ./docker-compose-build.sh heyne-digital.de
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To use the script to build images following the naming convention of the registry use the this line:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
IMAGE_SEPERATOR=/ IMAGE_PREFIX=docker.gitlab.gwdg.de/subugoe/archaeo18/ ./docker-compose-build.sh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Note:** Be aware that `docker-compose up` won't find the required images by default, they need to be 
explicitly named in the `docker-compose.yml` file(s).

### List of environment variables

| Name            | Default                        | Description                                           |
|-----------------|--------------------------------|-------------------------------------------------------|
| DOCKER          | None (lookup in PATH variable) | Path to the `docker` executable (if it's not on PATH) |
| GIT             | None (lookup in PATH variable) | Path to the `git` executable (if it's not on PATH)    |
| IMAGE_SEPERATOR | _                              | Separator for the image name                          |
| IMAGE_PREFIX    |  _(None)_                      | Prefix for the image name                             |

## Experimental features

The experimental mode builds two additional images, which can be used to work with the [experimental features](../docs/experimental.md#experimental).

For `git` needs to be on the path as well, otherwise it will fail. You can pass the location of the `git` executable 
using the environment variable `GIT`:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GIT=/opt/local/bin/git ./docker-compose-build.sh heyne-digital.de
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1    | Directory for Dockerfiles not found, could be started in the wrong directory                   |
| 2    | `docker` executable not found, use `DOCKER` variable                                           |
| 3    | `git` executable not found, use `GIT` variable (experimental mode)                             |

## Known issues

### `buildkit not supported by daemon`

If you're on Ubuntu follow the [guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/) to install a newer version of Docker.

### `signaling init process caused: access denied` on `docker-compose down`

This error is normally related to [App Armor](https://gitlab.com/apparmor/apparmor/wikis/home/), you can try to deactivate / disable it. If you want keep it for security 
reasons, look at the [documentation](https://docs.docker.com/engine/security/apparmor/) on running App Armor and Docker together.

## Building of additional images
The script also builds additional and experimental images. Some are blacklisted as they aren't part of te repository.
If you're experimenting with your own additions (updated base images for example), you can add the to the variable `BLACKLIST` 
in `docker-compose-build.sh`.
Additional variants (for production use for example) can be placed in the directories of their base images, if they are named 
with a suffix separated by a dot (.), they will be build after the base image the suffix will be appended to the tag name 
using a dash.

<a name="docker-compose_up"></a>
# Up (`docker-compose up`)

## Setting the entrypoint

If you need to to run the [`setup-nginx.sh`](../docs/web.md#setup-nginx) script you can use the `entrypoint` section in the `docker-compose.yml` 
file. The `ENTRYPOINT` defined by the `Dockerfile` exclude it by default.

**Note:** You don't need this magic if you use a mounted `nginx` configuration file!
You can use the `ENTRYPOINT` defined by the `Dockerfile` instead.

### Simple example

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  web:
    ...
    entrypoint:
      - /opt/archaeo18/change-host.sh
      - .
      - $DEPLOY_HOST_NAME
      - $ARCHAEO18_SCRIPTS/setup-nginx.sh
      - nginx
      - -g
      - daemon off;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This example just changes the host names in the files in the web root and linked parts of the `data` volume.
The default configuration file of `nginx` is altered to include the host name for deployment (`DEPLOY_HOST_NAME`).

### Example for production mode

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   web:
    ...
    entrypoint:
      - /opt/archaeo18/change-host.sh
      - .
      - heyne-digital.de
      - $ARCHAEO18_SCRIPTS/setup-nginx.sh "" /etc/archaeo18-nginx/site.prod.conf
      - nginx
      - -g
      - daemon off;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this example `setup-nginx.sh` is started with two arguments. The first (`""`) is the host name,
it can be empty in this case, it will be taken from the ENV variable `DEPLOY_HOST_NAME` in this case.
The second (`/etc/archaeo18-nginx/site.prod.conf`) is a alternative configuration file from within the container.
See the [section](../docs/web.md#nginx-config-files) on bundled `nginx` configurations. 

## Setting the webserver configuration

It is possible to override the bundled `nginx` configuration by mounting a use case specific one via `docker-compose.yml`, 
just add a section to the volume definition for the `web` container:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  web:
    ...
    volumes:
      ...
      - type: bind
        source: ./docker/web/conf/site.conf
        target: /etc/nginx/conf.d/default.conf
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Note on `data` container
The `data`container is used to create the shared `data` volume, so this container won't start during 
`docker-ompose up`. This is expected behaviour and not an error!

<a name="pre-build" ></a>
# Using pre-build images

The are pre-build [images](https://gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker/container_registry) build by GitLab on 
every new commit to the `master` branch. Due to limitations of GitLab these images have to follow another naming convention.
Where `docker-compose build` and [`docker-compose-build.sh`](docker-compose-build.sh) use underscores (`_`) the images provided 
by GitLab use a slash (`/`) in their name and not `latest` as version tag but the name of the branch (`master` in most cases). 
Example: `archaeo18-docker_web:latest` will become `archaeo18-docker/web:master`.
Additionally the GitLab registry has to be used as prefix: `docker.gitlab.gwdg.de/subugoe/`

Make sure you change your `docker-compose.yml` file(s) accordingly if you want to rely on the images.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  web:
    image: docker.gitlab.gwdg.de/subugoe/archaeo18/archaeo18-docker/web:master
    ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
