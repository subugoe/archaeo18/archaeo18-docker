<a name="data"></a>
The `data` image
====================

The `data` image is only used to encapsulate the data generation and enrichment process. 
It's a image on it's own to be able to exchange it easily with preprocessed data.

The base is [`alpine:3.10`](https://hub.docker.com/_/alpine).

The following dependencies are added to the image:

-  `busybox` - as a shell replacement.

To generate the data the following dependencies are needed:

-  `ant` and `openjdk8` - to use the enrichment scripts, keept after the build.

These aren't part of the image anymore, they are installed by the `generate-data.sh` script.

# TODO

-  Fix the native data generation using `ant`

# Build process

The data image is just used to build a volume for `web` and `search`, it's not runable by itself.
To allow changes of non root users, the contents are `chown` to `uid` 101 and `gid` 101.
To build the `data` image on its own run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_data -f docker/data/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Arguments

| Name                       | Default value                                          | Description                                                  |
|----------------------------|--------------------------------------------------------|--------------------------------------------------------------|
| GENERATE_DATA              | false                                                  | Should data be regenerated or taken from the `git`repository |

**Note**: Setting `GENERATE_DATA` to `true` is currently not implemented.

See also the [list of ENV vars](../README.md#env).

## Important paths and files inside the image

Applies after the image has been build

| Name                                                                  | Description                                                      |
|-----------------------------------------------------------------------|------------------------------------------------------------------|
| /data                                                                 | Root of the `data` volume                                        |
| /data/.data-created                                                   | Watch file for finished creation of data files                   |
| /data/content                                                         | Directory containing contents of `./source/content/app`          |
| /tmp/archaeo18.tar.bz2                                                | Archive containing the raw data files needed for data generation |
| /opt/data/generate-data.sh                                            | Experimental script to regenerate data from scratch              |

<a name="generate-data"></a>
# The `generate-data.sh` script

The `generate-data.sh` is called if `GENERATE_DATA` is set to `true` during the build. The purpose is to use `ant` 
to run the original Archaeo18 data generation tasks, which are [currently broken](https://gitlab.gwdg.de/subugoe/archaeo18/ropen-backend/issues/).
This script also installs the dependencies like Java and Ant, to do this it needs to be run as user `root`.

## Arguments

No arguments
