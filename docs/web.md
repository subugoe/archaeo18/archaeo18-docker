<a name="web"></a>
The `web` image
===================

The `web` image is providing NGinx instead of Apache as used by the currently running Heyne 
digital portal. 
The base is [nginx:stable-alpine](https://hub.docker.com/_/nginx). This image runs under Alpine Linux, including some dependencies:

- `nodejs`, `nodejs-npm`, `jq` and `yarn` - to build JavaScript related things, removed during build.

-  `git` - do get the source files, removed during build.

-  `busybox` - as a shell replacement.

The `web` container als acts as a proxy for the `search` and `images` containers, configuration is in `docker/web/conf/site.conf`.

**Note:** Since the proxing of the other containers, you need to adjust the Nginx configuration if you plan to run 
them on different ports!

**Note:** Using the preconfigured `ENTRYPOINT` the script [`change-host.sh`](./shared-scripts.md#change-host) is started first, 
after it has been finished the webserver is started. This can take some time, so make sure to look at the logs 
before tearing it down again.

# TODO

-  Run Nginx as unprivileged user - user is already configurable - currently postponed

# Build process

To build the `web` image on its own run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_web -f docker/web/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you just want to rebuild the `web` image for a different host name:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build --build-arg HOST_NAME=localhost:8009 -t archaeo18-docker_web -f docker/web/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Arguments

| Name                       | Default value                                          | Description                                                                                                           |
|----------------------------|--------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| HOST_NAME                  | localhost:8008                                         | The intended host name for deployment (can be changed on each startup by setting the env variable `DEPLOY_HOST_NAME`) |

See also the [list of ENV vars](../README.md#env).

## Important paths and files inside the image

Applies after the image has been build

| Name                                                                  | Description                                                      |
|-----------------------------------------------------------------------|------------------------------------------------------------------|
| /data                                                                 | Root of the `data` volume (files to serve)                       |
| /opt/archaeo18                                                        | Directory project specific scripts ($ARCHAEO18_SCRIPTS)          |
| /var/www/html                                                         | `$WORKDIR`                                                       |
| /var/www/html/public                                                  | Directory to serve files from (includes symlink to /data volume) |
| /etc/nginx/conf.d/default.conf                                        | Script to send files to the indexer                              |
| /cache                                                                | Root of the `cache` volume (production mode, `$PROXY_CACHE`)     |
| /etc/archaeo18-nginx/                                                 | Directory containing alternative configuration files             |
| /usr/lib/nginx/modules/                                               | Directory containing the Nginx modules                           |

# Web server configuration

The web sever is configured in `docker/web/conf/site.conf`. you need to change the name of the web server for production.
A second configuration file for production is provided (`docker/web/conf/site.prod.conf`), it supplies a cache of the `images` and `search` containers.
The currently preferred way to use these configurations is using `docker-compose`.

# Changes to files for hostnames

Additionally to the [`change-host.sh`](./shared-scripts.md#change-host) script several other files need to be changed for the host name

| Name                                                                               | Description                                                      |
|------------------------------------------------------------------------------------|------------------------------------------------------------------|
| /etc/nginx/conf.d/default.conf                                                     | Web Sever configuration (see [`setup-nginx.sh`](#setup-nginx))   |
| /var/www/html/public/ropen/Resources/Public/JavaScript/Config/EditionProperties.js | Application configuration (pfdLink etc.)                         |

# Scripts

<a name="setup-nginx"></a>
## `setup-nginx.sh`

This script is used to change the Nginx Configuration, either the default one or by the use of a template. 
These templates are stored in `docker/web/conf`, copied during the build to `/etc/archaeo18-nginx` and can be referenced from there 
as second parameter. Even though it will be checked if the default configuration file (`etc/nginx/conf.d/default.conf`), 
it's possible that write errors occur if the file is mounted by the Docker `bind` mechanism.

The `setup-nginx.sh` can be used to change two configuration settings in two config files:

| Configuration statement | File                           | Description                                                               |
|-------------------------|--------------------------------|---------------------------------------------------------------------------|
| server_name             | /etc/nginx/conf.d/default.conf | The host name the server listens for                                      |
| user                    | /etc/nginx/nginx.conf          | The user subprocesses of Nginx runs as, only available if started as root |

### Arguments

| Position    | Default value                  | Description                                                                         |
|-------------|--------------------------------|-------------------------------------------------------------------------------------|
| 1           | localhost:8008                 | Host name to run the web fronend on                                                 |
| 2           | /etc/nginx/conf.d/default.conf | Configuration file to use as template                                               |

### Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1    | Configuration file (default: `/etc/nginx/conf.d/default.conf`) not writeable                   |
| 2    | Change of configuration file (default: `etc/nginx/conf.d/default.conf`) couldn't be done       |


<a name="nginx-config-files"></a>
### Configuration file locations

Currently two configuration files are bundled during the build, the image locations can be used as second argument 
for `setup-nginx.sh`. Since the first is the default, it doesn't needs to be specified.
**Note**: It's always possible to override the configuration by mounting a configuration file using command line arguments or 
`docker-compose`, in that case this script will fail.

| Purpose                   | Repository location                  | Image location                      | Notes                                                                                     |
|---------------------------|--------------------------------------|-----------------------------------------|-------------------------------------------------------------------------------------------|
| Development / testing     | ./docker/web/conf/site.conf          | /etc/archaeo18-nginx/site.conf          | This is the default configuration file, it will be put in place by the build process      |
| Production                | ./docker/web/conf/site.prod.conf     | /etc/archaeo18-nginx/site.prod.conf     | This configuration will enable caching for the `image` and `search` containers            |
| Production (Experimental) | ./docker/web/conf/site.prod.zip.conf | /etc/archaeo18-nginx/site.prod.zip.conf | This configuration file can be used to serve static content from zip files (experimental) |
