<a name="shared"></a>
Shared scripts
==============

Shared scripts are stored in `docker/common/scripts`:

| Name                                                                  | Description                                                              |
|-----------------------------------------------------------------------|--------------------------------------------------------------------------|
| [`change-host.sh`](#change-host)                                      | This script searches for text files and replaces host names (and IPs)    |
| [`change-scheme.sh`](#change-scheme)                                  | This script searches for a scheme and replaces it                        |
| [`config.sh`](#config.sh)                                             | This script contains common configuration settings for the other scripts |


<a name="change-host"></a>
## `change-host.sh`

This script changes the host part of URLs including IP addresses, the adresses to search for are hard coded.
It puts the new changed host name in the file named by the variable `NEW_HOST_NAME_FILE`. 

| Host / IP                              | 
|----------------------------------------|
| 134.76.21.92:8080                      |
| 134.76.21.92                           |
| heyne-digital.tc.sub.uni-goettingen.de |

The search will include subdirectories and symbolic links.
If the source has already been changed (file named by the variable `NEW_HOST_NAME_FILE` exists), it will read the status file 
for the new name and uses it in the replacement part.

**Note:** This script will use other pathes to `grep`, `sed` and `find` on MacOS X fo debugging. In that case it assumes 
that [Mac Ports](https://www.macports.org/) GNU versions of these programs are installed to simulate their behaviour on Linux.  

The third parameter can be used to pass an additional script for changing things, it's currently used by [`setup-nginx.sh`](../docs/web.md#setup-nginx).

### Arguments

| Position    | Default value      | Description                                                                         |
|-------------|--------------------|-------------------------------------------------------------------------------------|
| 1           | .                  | Directory to search for files (including sub directories, following symbolic links) |
| 2           | localhost:8008     | Host name to run the web fronend on                                                 |
| 3           | ""                 | Command to execute during the change process                                        |
| 4           | $@                 | Commands to execute after the script has finished                                   |

### Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1XX  | Exit codes from argument 3 are prefixed with 100                                               |

<a name="change-scheme"></a>
## `change-scheme.sh`

This script changes the scheme part of URLs, it's purpose is to change it either to 'http' or 'https'. To make sure it only 
alters relevant URLs it looks if a file named by the variable `NEW_HOST_NAME_FILE` exists and if so, it will read the host 
name from there. this makes it dependent on [`change-host.sh`](#change-host). If the status file can't be read it will exit 
with an error.

### Arguments

| Position    | Default value      | Description                                                                         |
|-------------|--------------------|-------------------------------------------------------------------------------------|
| 1           | .                  | Directory to search for files (including sub directories, following symbolic links) |
| 2           | http://            | Scheme prefix                                                                       |

### Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1    | `NEW_HOST_NAME_FILE` doesn't exist                                                             |


<a name="config.sh"></a>
## `config.sh`

This script contains the following variables:

| Variable            | Description                                                          |
|---------------------|----------------------------------------------------------------------|
| HOST_NAMES          | The host names to search for (separated by space )                   |
| NEW_HOST_NAME       | The new host name (replacement)                                      |
| WORKING_DIR         | The directory to operate on                                          |
| NEW_HOST_NAME_FILE  | The name of the file to sto store the new host name in (status file) |

Furthermore it sets up the used commands:

| Variable |
|----------|
| FIND     |
| GREP     |
| SED      |

# Deployment scripts

Shared scripts are stored in `docker/common/deployment`:

| Name                                                                  | Description                                                              |
|-----------------------------------------------------------------------|--------------------------------------------------------------------------|
| [`deploy-master.sh`](#deploy-master)                                  | This script will be run during the deployment of the master branch       |

<a name="deploy-master"></a>
## `deploy-master.sh`

This script pulls new versions of images from the registry, stops the current running containers, cleans the volumes and 
starts containers from the new images.

### Arguments

None

### Exit codes

None, except those of the called programs.