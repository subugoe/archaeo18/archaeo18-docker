<a name="images"></a>
The `images` image
======================

The `images` image contains the images and provides required conversions and caching.
The base is [`docker-alpine:9`](https://hub.docker.com/_/jetty). This image runs under Alpine Linux, including:

-  `git` and `wget` - do get the source files, removed during build.

-  `ant` and `openjdk8` - to build the ContentServer , removed during build.

-  `busybox` - as a shell replacement.

Additionally the [Kitodo ContentServer](https://github.com/kitodo/kitodo-contentserver) (formerly Goobi ContentServer) is pulled from GitHub to serve the images.
**Note:** This image doesn't handle METS files, these need to be changed in the `data` image.
The images are in a special volume, called `images`.

The `images` image should be started with `JAVA_OPTIONS: "-Xmx2048m -Xms256m"` to limit it's memory usage.

# Build process

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_images -f docker/images/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Arguments

No Arguments, see also the [list of ENV vars](../README.md#env).

## Important paths and files inside the image

Applies after the image has been build

| Name                                                                  | Description                                           |
|-----------------------------------------------------------------------|-------------------------------------------------------|
| /images                                                               |  Root of the `images` volume                          |
| /images/orig                                                          |  Directory containing the full resolution images      |
| /images/cache                                                         |  Directory of the caches                              |
| /usr/local/jetty                                                      |  `$JETTY_HOME` directory of the Jetty installation    |
| /var/lib/jetty/webapps                                                |  `$JETTY_DEPLOY_DIR`, directory for webapp deployment |
| /var/lib/jetty/webapps/images                                         |  The location of the ContentServer App                |
| /var/lib/jetty/webapps/images/WEB-INF/web.xml                         |  Servlet configuration                                |
| /var/lib/jetty/webapps/images/WEB-INF/classes/contentServerConfig.xml |  ContentServer configuration                          |
| /var/lib/jetty/webapps/images/WEB-INF/classes/ehcache.xml             |  Cache Configuration                                  |
