<a name="search"></a>
The `search` image
======================

The `search` image started as provider of an ancient version of Solr (5.5). This was needed to 
match the proven version of the currently running Heyne digital portal. However at the current stae of things 
the version has become quite flexible, it's capable of running with version 5.5, 6.6 and 7.7. One can just 
replace the base image at the beginning of the [`Dockerfile`](./docker/search/Dockerfile)
The base image is a [`solr:[5-7]-slim`](https://github.com/docker-solr/docker-solr) image.

-  [Saxon HE](http://saxon.sourceforge.net/) is downloaded during the build as external dependency to allow Solr XSLT 2.0 tranformations. The latest version will be downloaded.

When the image is started the entrypoint script watches for a file (`$WATCH_FILE`) which is created by the `data` 
container. If it has been found, it checks if an index has already been created anf if not, it starts to transform the 
required files and sends them to Solr. Afterwards Solr will be started.

# Build process

During the build process the Solr core "Archeao18" is created and configured, Saxon is added.
To build the `search` image on its own run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DOCKER_BUILDKIT=1 docker build -t archaeo18-docker_search -f docker/search/Dockerfile .
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Arguments

No Arguments, see also the [list of ENV vars](../README.md#env).

## Important paths and files inside the image

Applies after the image has been build and is started

| Name                                                                  | Description                                                    |
|-----------------------------------------------------------------------|----------------------------------------------------------------|
| /data                                                                 | Root of the `data` volume (files to transform and index)       |
| /data/.data-created                                                   | Watch file for finished image creation (`$WATCH_FILE`)         |
| /index                                                                | Directory where the index itself is stored (`$SOLR_DATA_DIR`)  |
| /opt/archaeo18                                                        | Directory project specific scripts (`$ARCHAEO18_SCRIPTS`)      |
| /opt/archaeo18/post.sh                                                | Script to send files to the indexer                            |
| /opt/archaeo18/xslt                                                   | Directory project specific XSLT files                          |
| /opt/saxon                                                            | Saxon installation directory (`$SAXON_DIR`)                    |
| /opt/solr                                                             | `$SOLR_ROOT`, Solr root directory                              |
| /opt/solr/server/solr                                                 | `$SOLR_HOME`, Sorl home directory                              |
| /opt/solr/bin/solr.in.sh                                              | Solr configuration (to set XSLT processor)                     |
| /opt/solr/archaeo18                                                   | Solr core archaeo18                                            |
| /opt/solr/archaeo18/conf/                                             | Solr core archaeo18 configuration directory                    |
| /opt/solr/archaeo18/conf/xslt                                         | Solr core archaeo18 XSLT directory                             |

<a name="populate-index"></a>
# The `populate-index.sh` script

The `populate-index.sh` script will wait until a special file appears inside the `data` directory 
and starts to index the files after it has been found. If no file is found and an index exists, it starts the command
given as first parameter.

## Arguments

| Position    | Default value      | Description                                                                         |
|-------------|--------------------|-------------------------------------------------------------------------------------|
| 1           | $@                 | Commands to execute after the script has finished                                   |

See also the [list of ENV vars](../README.md#env). This script is used as entrypoint.

## Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1    | `DATADIR` not set or not found, files to index can't be found                                  |
| 2    | `SOLR_DATA_DIR` not set or not found, needed to check if the index already exists              |
| 3    | `IMPORT_DIR` not set or not found, and not extractable from compressed file                    |

<a name="update-solr"></a>
# The `update-solr.sh` script

The `update-solr.sh` script will use the `$SOLR_VERSION` environment variable to determine the required actions to update 
the core configuration to the desired Solr version. If an unsupported version is fount the exit code will be 1 to break the build.

## Current status

| Solr version   | Docker image   | Status                      | Current default |
|----------------|----------------|-----------------------------|-----------------|
| 5.5 and higher | solr:5-slim    | Tested and working          |                 |
| 5.5 and higher | solr:5.5       | Tested and working          |                 |
| 6.6 and higher | solr:6-slim    | Tested and working          |                 |
| 7.7 and higher | solr:7-slim    | Tested and working          |                 |
| 7.7 and higher | solr:7.7-slim  | Tested and working          |       ✗         |
| 8.2 and higher | solr:8-slim    | Implemented, but not tested |                 |

Since Solr 8.x uses the Java option `-XX:+UseLargePages` a error message is generated on startup. Docker seems to need a
higher shared memory setting.

## Arguments

No arguments, see also the [list of ENV vars](../README.md#env).

## Exit codes

| Code | Meaning                                                                                        |
|------|------------------------------------------------------------------------------------------------|
| 1    | No way to update the given Solr version, see error message                                     |
