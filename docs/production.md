<a name="production"></a>
Setting up a production instance
================================

# Quick start

**Note:** This section only applies to production mode!

Make sure you've checked out the sources before you start, this step is described in the development quick start guide.

## Building the images

**Note:** You don't need to configure anything yet. If things don't work without any configuration at this point, 
this is considered a bug!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
./docker-compose-build.sh new_host_name:port
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just replace _new_host_name:port_ with your desired host name and port.
If you don't give the new host name during the build it can be changed afterwards during the initialization of the `web` container.
From a clean checkout the script also changes the `.env` file to make sure this is the only change needed for building.
**But** you need to change the `dcoker-compose.yml` file(s) yourself.

## Configuring and starting the application

By default the `web` container uses a bundled configuration file, we also use this setup as part of the quick start guide.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up --no-build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Change the `web` configuration

If you run the `web` image with a mounted configuration file, you need to change `server_name` 
in `docker/web/conf/site.prod.conf` to match the name the server you're deploying the software on.

For advanced configuration options also see the [documentation on using `docker-compose`](../docs/docker-compose.md#docker-compose_up).

# Running with content caching for `images` and `search`

It's possible to start the `web` image with caching, just run:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up --no-build -d
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Make sure you've build the images beforehand.

Caching options are configured in `docker/web/conf/site.prod.conf`.

See the [document on `docker-compose up`](../docs/docker-compose.md#docker-compose_up) for ways to alter the configuration.

<a name="docker-compose.prod" />
# Setting up `docker-compose.yml`

To run all for deployment necessary scripts make sure that they are called.
This example includes the [`change-scheme.sh` script](../docs/shared-scripts.md#change-scheme) to deploy on 
a HTTPS enabled server.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  web:
    ...
    entrypoint:
      - /opt/archaeo18/change-host.sh
      - .
      - heyne-digital.de
      - $ARCHAEO18_SCRIPTS/setup-nginx.sh "" /etc/archaeo18-nginx/site.prod.conf && $ARCHAEO18_SCRIPTS/change-scheme.sh . https://
      - nginx
      - -g
      - daemon off;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Automatic deployment using GitLab

Make sure you have the 'Pipelines' and 'Container Registry' enabled as project features. Afterwards you can set additional
variables to pass different settings to your `.gitlab-ci.yml` file. For deployment you need something like the following:

| Name                   | Description                                     |
|------------------------|-------------------------------------------------|
| $PROD_SERVER_USER      | The user to login to the deployment host        |
| $PROD_HOSTNAME         | The host name of the deployment host            |
| $PROD_SERVER_DIRECTORY | The directory to start the deployment script in |
| $PROD_PRIVATE_KEY      | The private key to log into the deployment host |

How to set up the SSH keys is out of scope for this document, there will be another documentation for this topic.

The deployment script that will be copied to the server is documented in [another document](../docs/shared-scripts.md#deploy-master).

**Note:** The differences between the naming of the images build by the provided script and the GitLab CI apply here as well, 
you need to make sure that you are using the right naming convention.

**Important:** Since changing hostnames inside the `web` container and indexing inside the `search` container take some time there 
could be a downtime up till 10 minutes.

# Frequently asked questions

## Q: How do I serve my content over HTTPS?

**A:** You need to include [`change-scheme.sh` script](../docs/shared-scripts.md#change-scheme) in the entrypoint, for an example 
see [above](#docker-compose.prod). 